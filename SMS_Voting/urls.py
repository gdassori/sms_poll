from django.conf.urls import patterns, include, url
from base_app.views import poll, fetch_votes, build_poll
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^fetch_votes/$', fetch_votes),
    url(r'^iovoto/build_poll/$', build_poll),
    url(r'^iovoto/poll/$', poll)
)
