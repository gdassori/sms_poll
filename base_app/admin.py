from django.contrib import admin

# Register your models here.
from base_app.models import Poll, Votes

class VotesAdmin(admin.ModelAdmin):
    list_display = ('number', 'timestamp', 'vote')

class PollAdmin(admin.ModelAdmin):
    list_display = ('name', 'code')

admin.site.register(Poll, PollAdmin)
admin.site.register(Votes, VotesAdmin)