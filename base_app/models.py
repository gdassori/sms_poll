from django.db import models

# Create your models here.

class Votes(models.Model):
    timestamp = models.DateTimeField(null=True)
    number = models.CharField(max_length=100)
    vote = models.CharField(max_length=10, null=True)

class Poll(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=10)
