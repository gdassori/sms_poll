from django.shortcuts import render, HttpResponse
import hashlib
from django.views.decorators.csrf import csrf_exempt
from base_app.models import Votes, Poll

def build_poll(request):
    parties = dict(PD="Partito Democratico",
                   M5S="Movimento 5 Stelle",
                   FI="Forza Italia",
                   SVP="Sudtiroler Volkspartei",
                   TSI="L'altra Europa con Tsipras",
                   LEGA="Lega Nord",
                   FRAT="Fratelli d'Italia - Alleanza Nazionale",
                   NCD="Nuovo Centrodestra - UDC",
                   IDV="Italia dei Valori")
    for party in parties:
        poll = Poll.objects.get_or_create(name=parties[party], code=party)
        poll[0].save()
    return HttpResponse('built')

def poll(request):
    poll_parties = Poll.objects.all()
    parties = {}
    for i, v in enumerate(poll_parties):
        parties.update({poll_parties[i].code:[poll_parties[i].name,]})
    for party in parties:
        votes = Votes.objects.filter(vote=party)
        votes_i = len(votes)
        parties[party].append(votes_i)
    total_votes = 0
    for party in parties:
        total_votes += parties[party][1]
    for party in parties:
        percentage = (float(100) / float(total_votes)) * float(parties[party][1])
        parties[party].append(round(percentage, 2))
    print parties
    return render(request, 'poll.html', {'parties':parties, 'total_votes':total_votes})

@csrf_exempt
def fetch_votes(request):
    keyword = len('iovoto')
    if request.method == 'POST':
        body = request.POST.get('Body')
        timestamp = request.POST.get('Timestamp')
        md5_sender = hashlib.md5()
        md5_sender.update(str(request.POST.get('Sender')))
        sender = md5_sender.hexdigest()
        if sender and body and timestamp:
            try:
                vote = Votes.objects.get(number=sender)
                vote.timestamp = str(timestamp)
                vote.vote = str(body[keyword+1:]).upper()
                vote.save()
            except:
                try:
                    vote = Votes.objects.get_or_create(number=sender)
                    vote[0].timestamp = str(timestamp)
                    vote[0].vote = str(body[keyword+1:]).upper()
                    vote[0].save()
                except:
                    print 'Houston...'
                return HttpResponse('')
            return HttpResponse('')
        else:
            return HttpResponse('not enough')
    if request.method == 'GET':
        return HttpResponse('wtf?')
